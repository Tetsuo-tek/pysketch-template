#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow  import FlowChannel
from PySketch.flowsync      import FlowSync
from PySketch.flowproto     import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat       import FlowSat
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

###############################################################################

import time
import argparse

###############################################################################
# SKETCH

sat = FlowSat()

def setup():
    wrn("[SETUP] ..")

    parser = argparse.ArgumentParser(description="Nao publisher")
    parser.add_argument('sketchfile', help='Sketch program file')
    parser.add_argument('--user', help='Flow-network username', default='guest')
    parser.add_argument('--password', help='Flow-network password', default='password')
    args = parser.parse_args()
    
    sat.setLogin(args.user, args.password)

    t = 0.010 # seconds
    sat.setTickTimer(t, t * 50)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    
    sat.setStartChanPubReqCallBack(onStartChanPub)
    sat.setStopChanPubReqCallBack(onStopChanPub)

    sat.setGrabDataCallBack(onDataGrabbed)

    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:
        sat.setSpeedMonitorEnabled(True)
        wrn("[LOOP] ..")
    
    return ok

def loop():
    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    msg("Channel ADDED: {}".format(ch.name))

def onChannelRemoved(ch):
    msg("Channel REMOVED: {}".format(ch.name))

def onStartChanPub(ch):
    msg("Publish START required: {}".format(ch.name))

def onStopChanPub(ch):
    msg("Publish STOP required: {}".format(ch.name))

def onDataGrabbed(chanID, data):
    pass
    
###############################################################################
